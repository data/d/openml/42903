# OpenML dataset: physicochemical-protein

https://www.openml.org/d/42903

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Dataset**

This is a data set of Physicochemical Properties of Protein Tertiary Structure. The data set is taken from CASP 5-9. There are 45730 decoys and size varying from 0 to 21 armstrong.
	

**Attribute Description**

RMSD-Size of the residue.
F1 - Total surface area.
F2 - Non polar exposed area.
F3 - Fractional area of exposed non polar residue.
F4 - Fractional area of exposed non polar part of residue.
F5 - Molecular mass weighted exposed area.
F6 - Average deviation from standard exposed area of residue.
F7 - Euclidian distance.
F8 - Secondary structure penalty.
F9 - Spacial Distribution constraints (N,K Value).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42903) of an [OpenML dataset](https://www.openml.org/d/42903). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42903/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42903/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42903/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

